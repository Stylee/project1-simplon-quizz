<?php
session_start();
if (! isset($_SESSION['authenticated']))
{
    $_SESSION['authenticated'] = 'no';
} else {
    if ( $_SESSION['authenticated'] == 'yes')
	header('Location: choix-quizz.php?error_connexion=Vous êtes déjà connecté');
}

include 'connection_bdd.php';
?>

<!DOCTYPE html>
<html lang="fr">

    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	      integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/style.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<title>Le quiz des petits génies!</title>
    </head>
    
    <body>

	<?php include 'navbar.php';?>

	<div class="background-accueil">
	    <div class="container">
		<div class="row">
		    <div class="col-md-6 offset-md-3 col-sm-12" id="login">
			<section id="inner-wrapper" class="login">
			    <form method="post" action="choix-quizz.php?action=connection"> <!-- L'argument de l'url sert à rien là ??? --> 
				<?php
				if (isset($_GET['error_connexion']))
				{
				    $error = $_GET['error_connexion'];
				    echo "<p>$error</p>
				    ";
				}

				if (isset($_GET['error_creation']))
				{
				    $error = $_GET['error_creation'];
				    echo "<p>$error</p>
				    ";
				}

				?>

				<div class="form-group">
				    <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"> </i></span>
					<input type="text" class="form-control" name="nick" placeholder="Ton nom ou ton pseudo" required>
				    </div>
				</div>
				<div class="form-group">
				    <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-key"> </i></span>
					<input type="password" class="form-control" name="password" placeholder="Password" required>

					<button id="btn-connexion" type="submit" name="connexion" class="btn btn-success btn-block">
					    Connexion</button>
					<button id="btn-creation" type="submit" name="creation" class="btn btn-success btn-block">
					    Nouveau petit génie <div><small>(Créer un compte)</small></div></button>
				    </div>
				</div>

			    </form>
			</section>
		    </div>
		</div>
	    </div>
	</div>

	<?php include 'footer.php';?>
	
    </body>

</html>
