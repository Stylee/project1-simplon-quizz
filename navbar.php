<nav class="navbar dark-light bg-dark">

    <a href="/">
        <img src="assets/images/Logo.png" id="logo" class="d-inline-block align-top" alt="logo du site">
    </a>

    <div class="logo-central">

        <h2 class="cp-text">
            <span>K</span>
            <span>i</span>
            <span>d</span>
            <span>Q</span>
            <span>u</span>
            <span>i</span>
            <span>z</span>
            <span></span>
        </h2>

    </div>

    <div class="links">
	<a href="/tableau-des-scores.php" style="color:#FFD700;">
	    Tableau des scores</a>

	<a href="/quizz.php" style="color:#FFD700;">
	    Quizz
	</a>	
    </div>



    <?php
    if (isset($_SESSION['nick'])) {
        echo "
    <div>
	<p> Bienvenue <strong> $_SESSION[nick] </strong>!</p>

        <form action=\"logout.php\">
	      <button type=\"submit\" class=\"btn btn-outline-success\">Déconnecter</button>
        </form>
    </div>
	";
    }

    ?>
</nav>
