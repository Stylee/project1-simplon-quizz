<footer class="page-footer font-small special-color-dark pt-4" id="footer_quiz">
    <ul class="list-unstyled list-inline text-center">
        <li class="list-inline-item">
            <a class="btn-floating btn-fb mx-1" href="https://www.mstdn.fr/">
                <i class="fab fa-mastodon fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-tw mx-1" href="https://www.twitter.com/">
                <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-li mx-1" href="https://diasporafoundation.org/">
                <i class="fab fa-diaspora fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-ins-ic mx-1" href="https://www.instagram.com/">
                <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
            </a>
        </li>

    </ul>
    <div class="footer-copyright text-center py-3">
        © Kid's Quizz 2019 by Elodie et Marc Copyright</a>
    </div>

</footer>
