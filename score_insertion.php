<?php
session_start();
include 'connection_bdd.php';

$sql = "UPDATE resultats_quizz SET score = score + $_GET[score] WHERE themeID = $_GET[themeID] AND userID = $_GET[userID]";
$reponse = $bdd->query($sql);
$resultat = $reponse->execute();

$req = $bdd->prepare('INSERT INTO questions_answered(userID, themeID, questionID) VALUES(:userID, :themeID, :questionID)');
$req->execute(array(
    'userID' => $_GET['userID'],
    'themeID' => $_GET['themeID'],
    'questionID' => $_GET['questionID']
));
