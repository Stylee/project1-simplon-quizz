<?php
session_start();
include 'connection_bdd.php';

/* This method doesn't works (when the answer contains  ' for exemple)
  /* $sql = "SELECT * FROM quizz_questions WHERE id = '$_GET[questionID]' AND answer = '$_GET[answer]'";
 * $reponse = $bdd->query($sql);
 * $resultat = $reponse->fetch(); */

/* 2nd method */
$sql = $bdd->prepare("SELECT * FROM quizz_questions WHERE id = ? AND answer = ?");
$sql->bindValue(1, $_GET['questionID'], PDO::PARAM_INT);
$sql->bindValue(2, $_GET['answer'], PDO::PARAM_STR);
$sql->execute();
$resultat = $sql->fetch();

if ($resultat != null)
{
    echo "true";
} else {
    echo "false";
}
